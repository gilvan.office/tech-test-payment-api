using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PagamentoContext _context;

        public VendaController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (!venda.validaQuantidadeProduto())
                return BadRequest(new { Erro = "A venda deve possuir pelo menos um produto" });

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.Id }, venda);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            venda.Vendedor = _context.Vendedores.Where(x => x.Id == venda.Id).First<Vendedor>();
            venda.ProdutoVendido = _context.Produtos.Where(x => x.Id == venda.Id).ToList<Produto>();

            return Ok(venda);
        }

        [HttpPatch("{id}")]
        public IActionResult AtualizarVenda(int id, EnumStatus status)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            venda.Vendedor = _context.Vendedores.Where(x => x.Id == venda.Id).First<Vendedor>();
            venda.ProdutoVendido = _context.Produtos.Where(x => x.Id == venda.Id).ToList<Produto>();

            if (!venda.validaStatus(status))
                return BadRequest(new { Erro = "A venda não pode ser atualizada para este status" });

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }
    }
}