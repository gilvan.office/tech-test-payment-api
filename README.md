# SOLUÇÃO PARA O TESTE TÉCNICO POTTENCIAL   
Apresento minha solução para o Teste Técnico proposto pela Pottencial Seguradora em parceria com a Digital Innovation One através do bootcamp Pottencial .NET Developer.
##  INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-payment-api/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-payment-api/settings/members;
 - Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
 - Commit após cada ciclo de refatoração pelo menos;
 - Não use branches;
 - Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;

## O TESTE
- Construir uma API REST utilizando .Net Core, Java ou NodeJs (com Typescript);
- A API deve expor uma rota com documentação swagger (http://.../api-docs).
- A API deve possuir 3 operações:
  1) Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
  2) Buscar venda: Busca pelo Id da venda;
  3) Atualizar venda: Permite que seja atualizado o status da venda.
     * OBS.: Possíveis status: `Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`.
- Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
- O vendedor deve possuir id, cpf, nome, e-mail e telefone;
- A inclusão de uma venda deve possuir pelo menos 1 item;
- A atualização de status deve permitir somente as seguintes transições: 
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportador`. Para: `Entregue`
- A API não precisa ter mecanismos de autenticação/autorização;
- A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "em memória".

## PONTOS QUE SERÃO AVALIADOS
- Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Programação orientada a objetos;
- Boas práticas e princípios como SOLID, DDD (opcional), DRY, KISS;
- Testes unitários;
- Uso correto do padrão REST;   

## SOLUÇÃO PROPOSTA   

Para este teste técnico foi utilizado o C# com Entity Framework e banco de dados SQL Server. No terminal do Visual Studio Code, insere-se o seguinte comando para inicializar a API:   

```
dotnet new webapi
```   
 E para executá-la digita-se o comando:   

 ```
 dotnet watch run
 ```   

 A seguinte estrutura foi construída:   

 ![Estrutura de Pastas](Documentacao-Teste-Tecnico-API/Imagens/Estrutura.jpg)   

 - É constituída da pasta **_Models_**, que armazena todas as classes que representam as entidades de domínio;   
 - Da **_Controller_** que armazena as classes que agrupam as requisições HTTP e disponibiliza as endpoints da API;   
 - Pelas pastas **_Context_** e **_Migrations_** que armazenam as classes que lidam com o banco de dados;
 - E pelas pastas **_bin_**, **_obj_** e **_Properties_** onde são armazenadas as configurações-padrão de projeto webapi do C#.   

 ## Swegger   

 Essa é a documentação Swegger da aplicação, apresentando todos os endpoints e seus schemas:   

 ![Documentação Swegger](Documentacao-Teste-Tecnico-API/Imagens/API.jpg)   

 - **Schema Enums**   

  ![Schema do Enum](Documentacao-Teste-Tecnico-API/Imagens/SchemaEnum.jpg)   

  - **Schema Produto**   

 ![Schema do Produto](Documentacao-Teste-Tecnico-API/Imagens/SchemaProduto.jpg)   

  - **Schema Venda**   

 ![Schema de Venda](Documentacao-Teste-Tecnico-API/Imagens/SchemaVenda.jpg)   

 - **Schema Vendedor**   

 ![Schema de Vendedor](Documentacao-Teste-Tecnico-API/Imagens/SchemaVendedor.jpg)   

 ## Cenário de sucesso para cadastrar venda   

 Ao acessar o endpoint **_RegistraVenda_** e preencher todos os campos corretamente:   

 ![endpoint RegistraVenda](Documentacao-Teste-Tecnico-API/Imagens/RegistraVenda1.jpg)   

 A API retorna o **status code 201** e a venda cadastrada:   

 ![endpoint RegistraVenda](Documentacao-Teste-Tecnico-API/Imagens/RegistraVenda2.jpg)   

 Que também é refletida no banco de dados SQL Server:   

 ![Cadastro no banco de dados](Documentacao-Teste-Tecnico-API/Imagens/CadastroNoBanco.jpg)   

 ## Cenário de erro ao cadastrar venda   

 No cenário de erro, como digitar a quantidade do produto = 0,   

  ![Registro errado](Documentacao-Teste-Tecnico-API/Imagens/RegistroErrado1.jpg)   
  A API retorna o **status code 400** e a mensagem de erro: "A venda deve possuir pelo menos um produto":   


  ![Registro errado](Documentacao-Teste-Tecnico-API/Imagens/RegistroErrado2.jpg)   

  ## Cenário de sucesso para buscar venda   

  No endpoint **_BuscarVenda_** ao digitar o Id da venda cadastrada anteriormente:   

  ![Endpoint BuscaVenda](Documentacao-Teste-Tecnico-API/Imagens/BuscaVenda1.jpg)   

  A API retorna a venda cadastrada e o **status code 200**   

  ![Endpoint BuscaVenda](Documentacao-Teste-Tecnico-API/Imagens/BuscaVenda2.jpg)   

  ## Cenário de erro para buscar venda   

  Ao digitar um Id inexistente:   

  ![Endpoint BuscaVenda](Documentacao-Teste-Tecnico-API/Imagens/BuscaVenda3.jpg)   

  A API retorna o **status code 404**;   

  ![Endpoint BuscaVenda](Documentacao-Teste-Tecnico-API/Imagens/BuscaVenda4.jpg)   

  ## Cenário de sucesso para atualizar venda   

  No endpoint **_AtualizarVenda_**, basta digitar um Id existente e atualizar o status da venda de acordo com a regra de negócio:      

  ![Endpoint AtualizaVenda](Documentacao-Teste-Tecnico-API/Imagens/AtualizarVenda1.jpg)   

  A API retorna **status code 200** e o registro atualizado:   

  ![Endpoint AtualizaVenda](Documentacao-Teste-Tecnico-API/Imagens/AtualizarVenda2.jpg)   

  Se acessar o endpoint **_BuscarVenda_** e procurar o registro alterado:   

  ![Endpoint AtualizaVenda](Documentacao-Teste-Tecnico-API/Imagens/AtualizaVenda3.jpg)   

  A atualização é confirmada por ele:   

  ![Endpoint AtualizaVenda](Documentacao-Teste-Tecnico-API/Imagens/AtualizaVenda4.jpg)   

  E pelo banco de dados também:   

   ![Endpoint AtualizaVenda](Documentacao-Teste-Tecnico-API/Imagens/AtualizarVenda5.jpg)   

   ## Cenário de erro para atualizar venda   

   No cenário de erro, ao tentar atribuir um status indevido:   

  ![Atualização com Erro](Documentacao-Teste-Tecnico-API/Imagens/AtualizaComErro.jpg)   

  A API retorna o **status code 400** e a mensagem de erro: "A venda não pode ser atualizada para este status":   

  ![Atualização com Erro](Documentacao-Teste-Tecnico-API/Imagens/AtualizaComErro2.jpg)   

  E no caso do Id digitado for inexistente:   

  ![Atualização Inexistente](Documentacao-Teste-Tecnico-API/Imagens/AtualizaçãoInexistente1.jpg)   

  A API retornará o **status code 404**:   

   ![Atualização Inexistente](Documentacao-Teste-Tecnico-API/Imagens/AtualizaçãoInexistente2.jpg)      