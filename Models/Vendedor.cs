namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public int Id { get; set; }
        public string NomeVendedor { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public Vendedor()
        {

        }

        public Vendedor(int id, string nome, string cpf, string email, string telefone)
        {
            this.Id = id;
            this.NomeVendedor = nome;
            this.Cpf = cpf;
            this.Email = email;
            this.Telefone = telefone;
        }
    }
}