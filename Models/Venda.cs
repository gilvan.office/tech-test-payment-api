namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public List<Produto> ProdutoVendido { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }

        public EnumStatus Status { get; set; }

        public Venda()
        {

        }

        public Venda(int id, Produto produto, Vendedor vendedor)
        {
            this.Id = id;
            this.ProdutoVendido.Add(produto);
            this.Vendedor = vendedor;
            this.Data = DateTime.Now;
            this.Status = EnumStatus.Aguardando_Pagamento;
        }

        public bool validaQuantidadeProduto()
        {
            foreach (var produto in ProdutoVendido)
            {
                if (produto.Quantidade < 1)
                    return false;
            }
            return true;
        }

        public bool validaStatus(EnumStatus status)
        {
            var statusAprovado = (Status.Equals(EnumStatus.Aguardando_Pagamento)) && (status.Equals(EnumStatus.Pagamento_Aprovado));

            var statusAguardaCancela = (Status.Equals(EnumStatus.Aguardando_Pagamento)) && (status.Equals(EnumStatus.Cancelada));

            var statusEnviado = (Status.Equals(EnumStatus.Pagamento_Aprovado)) && (status.Equals(EnumStatus.Enviado_Para_Transportadora));

            var statusAprovaCancela = (Status.Equals(EnumStatus.Pagamento_Aprovado)) && (status.Equals(EnumStatus.Cancelada));

            var statusEntregue = (Status.Equals(EnumStatus.Enviado_Para_Transportadora)) && (status.Equals(EnumStatus.Entregue));


            if (statusAprovado || statusAguardaCancela || statusEnviado || statusAprovaCancela || statusEntregue)
            {
                this.Status = status;
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}