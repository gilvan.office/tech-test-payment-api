namespace tech_test_payment_api.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public string Nome { get; set; }
        public decimal PrecoUnitario { get; set; }

        public Produto()
        {

        }

        public Produto(int id, int quantidade, string nome, decimal preco)
        {
            this.Id = id;
            this.Quantidade = quantidade;
            this.Nome = nome;
            this.PrecoUnitario = preco;
        }
    }
}