namespace tech_test_payment_api.Models
{
    public enum EnumStatus
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_Para_Transportadora,
        Entregue,
        Cancelada
    }
}